import {useState, useEffect} from 'react';

import classNames from "./Table.module.css";

const Table = ({ columns, rows, types }) => {
    const [data, setData] = useState(rows);
    const [filerState, setFilter] = useState({});
    const [sortConfig, setSortConfig] = useState(null);

    useEffect(() => {
        let filteredArray = rows.filter((item) => {
            for (let key in filerState){
                if(item[key] === undefined || !item[key].toString().toUpperCase().includes(filerState[key]))
                      return false
                }

            return  true
        })

        setData(filteredArray)

    }, [filerState,rows])

    useEffect(() => {
        let sortableItems =[...data];

        if(sortConfig !== null){
            sortableItems.sort((a, b)=> {
                if (a[sortConfig.key] < b[sortConfig.key])
                    return sortConfig.direction === 'asc' ? -1 : 1

                if(a[sortConfig.key] > b[sortConfig.key])
                    return sortConfig.direction === 'asc' ? 1 : -1

                return 0
            })
        }

        setData(sortableItems)
        // eslint-disable-next-line
    },[sortConfig])

    const handleChange = (searchValue, id) =>
        setFilter(state => ({...state, [id]: searchValue.toString().toUpperCase()}))

    const getClassNamesFor = (key) => {
        if (!sortConfig) return

        let className = sortConfig.direction === 'asc' ? classNames.asc : classNames.dsc
        return sortConfig.key === key ? className : undefined;
    };

    const requestSort = (key) => {
        let direction = 'asc';
        if (
            sortConfig &&
            sortConfig.key === key &&
            sortConfig.direction === 'asc'
        ) {
            direction = 'dsc';
        }

        setSortConfig({ key, direction });
    };

    return (
    <table title="Movies" className={classNames.table}>
      <thead>
        <tr>
          {columns.map((item) => (
              <th key={item.id}>
                  <button
                      type={"button"}
                      className={getClassNamesFor(item.id)}
                      onClick={() =>requestSort(item.id)}
                  >
                  {item.title}
                  </button>
              </th>
              )
          )}
      </tr>

        <tr>
          {columns.map(({ id }) => (
              <th key={id}>
                  <input
                      placeholder={`search in ${id}`}
                      onChange={(e) => handleChange(e.target.value, `${id}`)}
                  />
              </th>
              )
          )}
      </tr>
      </thead>

      <tbody>
        {data.map((row, index) => (
            <tr key={index}>
                {columns.map(({ id }) => (
                    <td
                        data-testid={`row-${index}-${id}`}
                        className={classNames[`cell-type-${types[id]}`]}
                        key={id}
                    >
                        {row[id]}
                    </td>
                    )
                )}
            </tr>
            )
        )}
      </tbody>
    </table>
  );
};

export default Table;

